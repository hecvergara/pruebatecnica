package co.com.pruebatecnica.stepsdefinition.stepeliminarregistro;

import co.com.pruebatecnica.business.ControllerEliminarRegistro;
import co.com.pruebatecnica.datatransferobject.DataRespuestaServicio;
import co.com.pruebatecnica.logs.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.nio.file.Paths;

public class StepDefinition {
    private String endPoint;
    private String path;
    private DataRespuestaServicio respuestaServicio;
    private ControllerEliminarRegistro eliminarRegistro;

    @Given("^que se tiene una api de pruebas$")
    public void queSeTieneUnaApiDePruebas() {
        Log.initLogs(Paths.get(System.getProperty("user.dir"),"EVIDENCIAS"), "ELIMINAR");
        Log.LOGGER.info("***********************************************************************");
        Log.LOGGER.info("************************ Inicio prueba ********************************");
        endPoint = "http://localhost:3000";
        path = "/usuario";
        Log.LOGGER.info("El servicio que se va a probar es: " + endPoint.concat(path));

    }

    @When("^cuando se elimine un registro por id (.*?)$")
    public void cuandoSeElimineUnRegistro(String id) {
        eliminarRegistro = new ControllerEliminarRegistro();
        respuestaServicio = eliminarRegistro.eliminarRegistro(endPoint, path + "/" + id);
        Log.LOGGER.info("El codigo de  respuesta que se obtuvo es: " + respuestaServicio.getStatusCode());
        Log.LOGGER.info("El response que se obtuvo es: \n" + respuestaServicio.getBody());
    }

    @Then("^entonces el codigo de respuesta debe ser igual al esperado (.*?)$")
    public void entoncesElCodigoDeRespuestaDebeSerIgualAlEsperado(String codigoEsperado) {
        Assert.assertEquals(respuestaServicio.getStatusCode(), codigoEsperado);
        Log.LOGGER.info("************************ Finalizó prueba *****************************");
        Log.LOGGER.info("**************************************** *****************************");
    }
}

package co.com.pruebatecnica.stepsdefinition.stepconsultarrelacioncolegios;

import co.com.pruebatecnica.business.ConsultarRegistros;
import co.com.pruebatecnica.business.ControllerInsertarRegistro;
import co.com.pruebatecnica.logs.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.nio.file.Paths;
import java.util.List;

public class StepDefinitioon {
    private ConsultarRegistros consultarRegistros;
    private String endPoint;
    private String pathColegio;
    private String pathUsuario;
    private List<String> listUsuarios;

    @Given("^que tenemos los servicios de consulta colegio y usuario$")
    public void queTenemosLosServiciosDeConsultaColegioYUsuario() {
        Log.initLogs(Paths.get(System.getProperty("user.dir"), "EVIDENCIAS"), "CONSULTAR");
        Log.LOGGER.info("**********************************************************************");
        Log.LOGGER.info("************************ Inicio prueba********************************");
        endPoint = "http://localhost:3000";
        pathColegio = "/colegio/?_start=0&_end=2";
        pathUsuario = "/usuario";
        Log.LOGGER.info("El servicio que se va a probar es: " + endPoint.concat(pathColegio));
        Log.LOGGER.info("El servicio que se va a probar es: " + endPoint.concat(pathUsuario));
    }

    @When("^consultemos la relacion entre usuario colegio$")
    public void consultemosLaRelacionEntreUsuarioColegio() {
        consultarRegistros = new ConsultarRegistros();
        listUsuarios = consultarRegistros.buscarColegioEnUsuario(endPoint, pathColegio, pathUsuario);
    }

    @Then("^se mostraran los usuarios relacionados con los colegios$")
    public void seMostraranLosUsuariosRelacionadosConLosColegios() {
        for (String jsonPersona : listUsuarios) {
            Log.LOGGER.info("La persona es: " + jsonPersona);
        }
    }
}

package co.com.pruebatecnica.stepsdefinition.stepinsertarregistro;

import co.com.pruebatecnica.business.ControllerInsertarRegistro;
import co.com.pruebatecnica.datatransferobject.DataRespuestaServicio;
import co.com.pruebatecnica.logs.Log;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.nio.file.Paths;

public class StepDefinition {
    private ControllerInsertarRegistro controllerInsertarRegistro;
    private String endPoint;
    private String path;
    private DataRespuestaServicio respuestaServicio;

    @Given("^que tenmos una api de pruebas$")
    public void queTenmosUnaApiDePruebas() {
        controllerInsertarRegistro = new ControllerInsertarRegistro();
        Log.initLogs(Paths.get(System.getProperty("user.dir"),"EVIDENCIAS"), "INSERTAR");
        Log.LOGGER.info("**********************************************************************");
        Log.LOGGER.info("************************ Inicio prueba********************************");
        endPoint = "http://localhost:3000";
        path = "/usuario";
        controllerInsertarRegistro.dataInicialLista(endPoint,path);

        Log.LOGGER.info("El servicio que se va a probar es: " + endPoint.concat(path));
    }

    @When("^insertemos datos a la coleccion (.*?), (.*?), (.*?), (.*?)$")
    public void insertemosDatosALaColeccion(String nombre1, String nombre2, String apellido1, String apellido2) {
        String request = controllerInsertarRegistro.prepararRequest(nombre1, nombre2, apellido1, apellido2);
        Log.LOGGER.info("El Request que se va a ejecutar es: \n" + request);
        respuestaServicio = controllerInsertarRegistro.insertarRegistro(endPoint, request, path);
        Log.LOGGER.info("El codigo de  respuesta que se obtuvo es: " + respuestaServicio.getStatusCode());
        Log.LOGGER.info("El response que se obtuvo es: \n" + respuestaServicio.getBody());
    }

    @Then("^la informacion sera alamcenada$")
    public void laInformacionSeraAlamcenada() {
        Assert.assertEquals(respuestaServicio.getStatusCode(), "201");
        Log.LOGGER.info("************************ Finalizó prueba *****************************");
        Log.LOGGER.info("**********************************************************************");
    }
}

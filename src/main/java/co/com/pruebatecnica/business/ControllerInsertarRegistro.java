package co.com.pruebatecnica.business;

import co.com.pruebatecnica.datatransferobject.DataRespuestaServicio;
import co.com.pruebatecnica.logs.Log;
import com.jayway.jsonpath.JsonPath;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static co.com.pruebatecnica.business.ControllerEliminarRegistro.consultarPersona;
import static io.restassured.RestAssured.given;

public class ControllerInsertarRegistro {

    public DataRespuestaServicio insertarRegistro(String endPoint, String request, String path) {
        DataRespuestaServicio respuestaServicio = new DataRespuestaServicio();
        try {
            RestAssured.baseURI = endPoint;
            RequestSpecification requestSpecification = given();
            requestSpecification.header("Content-Type", "application/json");
            requestSpecification.body(request);
            Response response = requestSpecification.post(path);
            respuestaServicio.setStatusCode(String.valueOf(response.getStatusCode()));
            respuestaServicio.setBody(response.getBody().prettyPrint());
        } catch (Exception e) {
            Log.LOGGER.info("Error consumiendo el servicio:", e);
            Log.LOGGER.info("************************ Finalizó prueba *****************************");
            Log.LOGGER.info("*********************************************************************");
        }
        return respuestaServicio;
    }

    public static int generarRamdon(int limiteSuperior) {
        return (int) (Math.random() * limiteSuperior + 1);
    }

    public static String prepararRequest(String nombre1, String nombre2, String apellido1, String apellido2) {
        String idColegio = String.valueOf(generarRamdon(3));

        return "{\n" +
                "    \"primerNombre\": \"" + nombre1 + "\",\n" +
                "    \"segundoNombre\": \"" + nombre2 + "\",\n" +
                "    \"primerApellido\": \"" + apellido1 + "\",\n" +
                "    \"segundoApellido\": \"" + apellido2 + "\",\n" +
                "    \"id_Colegio\": " + idColegio + "\n" +
                "  }";
    }

    public void dataInicialLista(String endPonit, String path) {
        ControllerEliminarRegistro eliminarRegistro = new ControllerEliminarRegistro();
        String json = consultarPersona(endPonit,path);
        int cantidadRegistros = JsonPath.read(json, "$.length()");
        if (cantidadRegistros > 8) {
            for (int i = 7; i < cantidadRegistros; i++) {
                int valorId = JsonPath.read(json, "$.[" + i + "].id");
                String id = String.valueOf(valorId);
                eliminarRegistro.eliminarRegistro(endPonit, path + "/" + id);
            }
        } else {
            Log.LOGGER.info("No es necesario preparar la data Usuario, con tiene los 7 registros iniciales!");
        }
    }

}

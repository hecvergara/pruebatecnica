package co.com.pruebatecnica.business;

import co.com.pruebatecnica.logs.Log;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import static co.com.pruebatecnica.business.ControllerEliminarRegistro.consultarPersona;

public class ConsultarRegistros {

    public List<String> buscarColegioEnUsuario(String endPoint, String pathColegio, String pathUsuario) {
        List<String> listUsuarios = new ArrayList<>();
        String json = consultarPersona(endPoint, pathColegio);
        Log.LOGGER.info("Los 2 primeros colegios son: " + json);
        int cantidadRegistros = JsonPath.read(json, "$.length()");
        for (int i = 0; i < cantidadRegistros; i++) {
            int valorId = JsonPath.read(json, "$.[" + i + "].id");
            String jsonUsuarios = consultarPersona("http://localhost:3000", pathUsuario);
            JSONArray array = JsonPath.read(jsonUsuarios, "$.[?(@.id_Colegio=='" + valorId + "')]");
            for (int j = 0; j < array.size(); j++) {
                listUsuarios.add(array.get(j).toString());
            }
        }
        return listUsuarios;
    }

}

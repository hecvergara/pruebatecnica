package co.com.pruebatecnica.business;

import co.com.pruebatecnica.datatransferobject.DataRespuestaServicio;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class ControllerEliminarRegistro {

    public static String consultarPersona(String endPoint, String path) {
        RestAssured.baseURI = endPoint;
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.get(path);
        return response.asString();
    }

    public DataRespuestaServicio eliminarRegistro(String endPoint, String path) {
        DataRespuestaServicio respuestaServicio = new DataRespuestaServicio();
        RestAssured.baseURI = endPoint;
        RequestSpecification httpRequest = RestAssured.given();
        Response response = httpRequest.delete(path);
        respuestaServicio.setBody(response.getBody().toString());
        respuestaServicio.setStatusCode(String.valueOf(response.getStatusCode()));
        return respuestaServicio;
    }


}

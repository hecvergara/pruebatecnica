Feature: Eliminar registro Usuario

  Scenario Outline: Eliminar registro en array Usuario
    Given que se tiene una api de pruebas
    When cuando se elimine un registro por id <Id>
    Then entonces el codigo de respuesta debe ser igual al esperado <CodigoEsperado>

    Examples:
      | Id  | CodigoEsperado |
      | 8   | 200            |
      | 120 | 404            |
